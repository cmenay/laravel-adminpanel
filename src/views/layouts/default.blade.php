@extends('layouts.app')

@section('title', 'Planes de Acción')

@section('content')



	@foreach ($css as $url)
		<link href="{{$url}}" media="all" type="text/css" rel="stylesheet">
	@endforeach

	<!--[if lte IE 9]>
		<link href="{{asset('packages/frozennode/administrator/css/browsers/lte-ie9.css')}}" media="all" type="text/css" rel="stylesheet">
	<![endif]-->


		@include('administrator::partials.header')

		{!! $content !!}



	@foreach ($js as $url)
		<script src="{{$url}}"></script>
	@endforeach


@endsection